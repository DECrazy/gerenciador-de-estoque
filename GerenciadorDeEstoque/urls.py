"""GerenciadorDeEstoque URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from stock.views import *
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', ProdutosListView.as_view(), name='Produtos-list'),
    path('pesquisa/', SearchView.as_view(), name='search'),
    path('cadastrar/', ProdutosCreateView.as_view(), name='cadastro'),
    path('reestocar/', SemEstoqueListView.as_view(), name='sem_estoque'),
    path('adicionar-estoque/<int:pk>/', AddStockView.as_view(), name='add-stock'),
    path('remover-estoque/<int:pk>/', RemoveStockView.as_view(), name='remove-stock'),
    path('usuarios/', include('django.contrib.auth.urls')),
    
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
